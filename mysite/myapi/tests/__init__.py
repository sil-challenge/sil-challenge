from decouple import config

username = config('AT_USERNAME')
api_key = config('AT_API_KEY')